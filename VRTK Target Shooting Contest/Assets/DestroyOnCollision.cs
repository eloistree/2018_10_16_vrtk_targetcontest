﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DestroyOnCollision : MonoBehaviour {

    public LayerMask m_interactableLayer;
    public string [] m_possibleTags;
    public UnityEvent m_beforeDestroy;

    private void OnCollisionEnter(Collision collision)
    {
        DestroyIfBelongTo(collision.collider);
    }
    private void OnTriggerEnter(Collider other)
    {
        DestroyIfBelongTo(other);

    }

    private void DestroyIfBelongTo(Collider other)
    {
        if (BelongTo(other))
        {
            m_beforeDestroy.Invoke();
            Destroy(gameObject);
        }
    }

    private bool BelongTo(Collider other)
    {
        return BelongTo(other.gameObject.layer, m_interactableLayer) || BelongTo(other.gameObject.tag, m_possibleTags);
    }

    public bool BelongTo(int layer, LayerMask maskLayer) {
        return IsInLayerMask(layer, maskLayer);
    }
    public bool BelongTo(string tag, string [] tags) {
        return Array.IndexOf(tags, tag) >= 0; 
    }

    public static bool IsInLayerMask(int layer, LayerMask layermask)
    {
        return layermask == (layermask | (1 << layer));
    }


}
