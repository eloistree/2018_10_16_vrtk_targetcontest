﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveInDirection : MonoBehaviour {

    public Vector3 m_direction= Vector3.forward;
    public Space m_spaceDirection= Space.Self;
    public float m_speed=2;
    
	void Update () {
        transform.Translate(m_direction * m_speed * Time.deltaTime, m_spaceDirection);
		
	}
}
