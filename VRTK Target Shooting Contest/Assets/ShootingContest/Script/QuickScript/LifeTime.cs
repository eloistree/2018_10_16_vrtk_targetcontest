﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LifeTime : MonoBehaviour {

    public float m_timeLeft=5;
    public UnityEvent m_beforeDestroy;
	void Update () {
        if (m_timeLeft <= 0)
            return;

        m_timeLeft -= Time.deltaTime;
        if (m_timeLeft < 0)
        {
            m_timeLeft = 0;
            m_beforeDestroy.Invoke();
            Destroy(this.gameObject);
        }
	}


}
