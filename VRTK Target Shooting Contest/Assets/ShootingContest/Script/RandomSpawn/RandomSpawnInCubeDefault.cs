﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnInCubeDefault : RandomSpawnInCube {

    public GameObject m_prefab;
    public override GameObject InstanciateSpawnable()
    {
        GameObject created=null; 
        if(m_prefab!=null)
            created= Instantiate(m_prefab);
        else
            created = new GameObject("No Prefab Defined");
        return created;
    }
    
}
