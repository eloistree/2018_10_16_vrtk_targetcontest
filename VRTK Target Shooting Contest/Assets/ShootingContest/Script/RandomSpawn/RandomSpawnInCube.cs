﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RandomSpawnInCube : MonoBehaviour {
    
    public Transform m_cubeTarget;
    public bool m_withOriantation;

    public float m_minDelay=0.5f;
    public float m_maxDelay=2f;
    IEnumerator Start () {
        while (true) {
            Spawn();
            yield return new WaitForSeconds(UnityEngine.Random.Range(m_minDelay, m_maxDelay));
        }
	}

    public  abstract GameObject InstanciateSpawnable();

    public  GameObject Spawn()
    {
        GameObject createdObject = InstanciateSpawnable();
        createdObject.transform.position = GetRandomPosition();
        if (m_withOriantation)
            createdObject.transform.rotation = m_cubeTarget.rotation;

        createdObject.SetActive(true);
        return createdObject;
    }

  

    public Vector3 GetRandomPosition() {
        Vector3 position = m_cubeTarget.position;
        Vector3 localPosition = m_cubeTarget.localScale;
        localPosition.y *= UnityEngine.Random.Range(-1f, 1f);
        localPosition.z *= UnityEngine.Random.Range(-1f, 1f);
        localPosition.x *= UnityEngine.Random.Range(-1f, 1f);
        localPosition /= 2f;
        Vector3 rotatedPosition = m_cubeTarget.rotation*localPosition;
        position += rotatedPosition;
        return position;

    }

    public void Reset()
    {
        m_cubeTarget = transform;
    }
}
